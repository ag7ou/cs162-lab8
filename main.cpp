// Rob Johnson - lab 8 - pcc cs162 - 11/29/18

#include <iostream>
#include <cstring>

using namespace std;

#define MAX_STRLEN 201

struct student
{
    unsigned int id;
    char name[MAX_STRLEN];
    float grade;
};

struct node
{
    student data;
    node* next;
};

void deleteNode(node* &head, unsigned int delete_value)
{
    node* current = head;
    node* previous = NULL;

    while(current)
    {
        if(current->data.id == delete_value)
        {
            // found match - delete
            if(previous == NULL)    // head of list
            {
                head = current->next;
            }
            else                    // any other node
            {
                previous->next = current->next;
            }

            delete current;
            break;
        }
        else    // no match - increment to next node
        {
            previous = current;
            current = current->next;
        }
    }
}

void deleteList(node* &head)
{
    while(head)
    {
        node* next = head->next;
        delete head;
        head = next;
    }
}

void addNode(node* &head, student data)
{
    node *newnode = new node;
    newnode->data.id = data.id;
    newnode->data.grade = data.grade;
    strcpy(newnode->data.name, data.name);

    if(head)
    {
        node* current = head;
        node* previous = NULL;

        while(current)
        {
            if(newnode->data.id > current->data.id)
            {
                if(current->next == NULL)   // last position
                {
                    current->next = newnode;
                    newnode->next = NULL;
                    break;
                }
                else    // increment to next
                {
                    previous = current;
                    current = current->next;
                }
            }
            else    // newnode->data <= current->next
            {
                if(previous == NULL) // first position
                {
                    head = newnode;
                    newnode->next = current;
                    break;
                }
                else // falls between 2 records
                {
                    previous->next = newnode;
                    newnode->next = current;
                    break;
                }
            }
        }
    }
    else    // fist node of list
    {
        head = newnode;
        head->next = NULL;
    }
}

void printList(node* head)
{
    node* current = head;

    while(current)
    {
        cout << "ID: " << current->data.id << endl;
        cout << "Name: " << current->data.name << endl;
        cout << "grade: " << current->data.grade << endl;
        cout << "********************" << endl;
        current = current->next;
    }
}

student input_student_data(void)
{
    student data;

    cin.ignore();

    cout << "Name: ";
    cin.getline(data.name, MAX_STRLEN);
    cout << "ID: ";
    cin >> data.id;
    cout << "grade: ";
    cin >> data.grade;

    return data;
}

void menu(node* &head)
{
    unsigned int selection = 0;

    while(selection != 4)
    {
        cout << "\n---Menu---\n1 Add student\n2 Print all\n3 Delete student\n4 Exit\nSelection: ";
        cin >> selection;

        switch(selection)
        {
            case 1:
            {
//                add student
                student newdata = input_student_data();
                addNode(head, newdata);
                break;
            }
            case 2:
            {
//                print all
                cout << "*** Class List ***\n******************" << endl;
                printList(head);
                break;
            }
            case 3:
            {
//                delete
                unsigned int id;
                cout << "Enter ID of student to delete: " << endl;
                cin >> id;
                deleteNode(head, id);
                break;
            }
            case 4:
            {
                cout << "Exiting" << endl;
                break;
            }
            default:
            {
                cout << "Invalid entry. Try again." << endl;
                break;
            }
        }
    }
}

int main(void)
{
    node* head = NULL;

    cout << "=======================\n*** CLASS LIST ***" << endl;

    menu(head);
    deleteList(head);

    cout << "\\=========\\" << endl;

    return 0;
}
